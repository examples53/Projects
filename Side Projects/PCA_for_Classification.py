#Mahima Hasmani

# PCA for Classification
# Explored 3 classifiers: K-Means, Binary Classification using Neural Networks, Logistic Regression

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.cluster import KMeans
from sklearn.metrics import accuracy_score
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from sklearn.decomposition import PCA


data = pd.read_csv ('heart.csv')

###################### One-Hot Encoding of 'famhist' ######################
OHE = pd.get_dummies(data['famhist'])
data = data.drop('famhist', axis=1)
data = data.join(OHE)


X = data.drop(columns = 'chd')
y = data['chd']

########## K-Means clustering for Raw Data ##########

X_train, X_test, y_train, y_test = train_test_split (X,y, test_size = 0.5, random_state=45)

sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

#StandardScaler generates a numpy array for X_train and X_test without headers so I converted back to dataframe so the headers would show. This is useful to me for generating the plots
X_train = pd.DataFrame(X_train, columns=['row.names', 'sbp', 'tobacco', 'ldl', 'adiposity', 'typea', 'obesity', 'alcohol', 'age', 'Absent', 'Present'])
X_test = pd.DataFrame(X_test, columns=['row.names', 'sbp', 'tobacco', 'ldl', 'adiposity', 'typea', 'obesity', 'alcohol', 'age', 'Absent', 'Present'])
y_train = pd.DataFrame (y_train, columns = ['chd'])
y_test = pd.DataFrame (y_test, columns = ['chd'])

km = KMeans (n_clusters = 2, random_state=42)
km.fit(X_train, y_train)
y_pred = km.predict(X_test)
y_pred = pd.DataFrame (y_pred, columns = ['chd']) #converting y_pred as numpy array to dataframe

# Q1) Accuracy

print('Accuracy Score (%) ', accuracy_score(y_test, y_pred)*100) 

# Q2) Actual Data Points

sbp_0 = []
sbp_1 = []
tob_0 = []
tob_1 = []

result = pd.concat([X_test, y_test], axis=1)  #For actual data points

w = result.loc[result['chd']==0, 'sbp']
sbp_0.append(w)

w = result.loc[result['chd']==1, 'sbp']
sbp_1.append(w)

w = result.loc[result['chd']==0, 'tobacco']
tob_0.append(w)

w = result.loc[result['chd']==1, 'tobacco']
tob_1.append(w)

plt.scatter (tob_0, sbp_0, color='g',  label = 'chd = 0' )
plt.scatter (tob_1, sbp_1, color='b', label = 'chd = 1' )
plt.title ('Standardized Plot of sbp vs. tobacco: Actual Data Points')
plt.xlabel ('tobacco')
plt.ylabel ('sbp')
plt.legend()

# Q3) Predicted Data Points

sbp_0_pred = []
sbp_1_pred = []
tob_0_pred = []
tob_1_pred = []

result = pd.concat([X_test, y_pred], axis=1)  #For predicted data points

w = result.loc[result['chd']==0, 'sbp']
sbp_0_pred.append(w)

w = result.loc[result['chd']==1, 'sbp']
sbp_1_pred.append(w)

w = result.loc[result['chd']==0, 'tobacco']
tob_0_pred.append(w)

w = result.loc[result['chd']==1, 'tobacco']
tob_1_pred.append(w)

plt.figure()
plt.scatter (tob_0_pred, sbp_0_pred, color='g',  label = 'chd = 0' )
plt.scatter (tob_1_pred, sbp_1_pred, color='b', label = 'chd = 1' )
plt.title ('Standardized Plot of sbp vs. tobacco: Predicted Data Points')
plt.xlabel ('tobacco')
plt.ylabel ('sbp')
plt.legend()

########## Binary Classification using Neural Network ##########

data = pd.read_csv ('heart.csv')

# One-Hot Encoding of 'famhist' 
OHE = pd.get_dummies(data['famhist'])
data = data.drop('famhist', axis=1)
data = data.join(OHE)

X = data.drop(columns = 'chd')
y = data['chd']

y = tf.one_hot(y, 2) 
y = pd.DataFrame (y, columns=['chd_OHE0', 'chd_OHE1']) #changing y back to a dataframe to allow for train/test split

X_train, X_test, y_train, y_test = train_test_split (X,y, test_size = 0.5, random_state=45)
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

#StandardScaler generates a numpy array for X_train and X_test without headers so I converted back to dataframe so the headers would show. This is useful to me for generating the plots
X_train = pd.DataFrame(X_train, columns=['row.names', 'sbp', 'tobacco', 'ldl', 'adiposity', 'typea', 'obesity', 'alcohol', 'age', 'Absent', 'Present'])
X_test = pd.DataFrame(X_test, columns=['row.names', 'sbp', 'tobacco', 'ldl', 'adiposity', 'typea', 'obesity', 'alcohol', 'age', 'Absent', 'Present'])
#y_train = pd.DataFrame (y_train, columns = ['chd_OHE0, chd_OHE1'])
#y_test = pd.DataFrame (y_test, columns = ['chd_OHE0', 'chd_OHE1'])


model = Sequential()
model.add(Dense(500, input_shape=(X_train.shape[1],), activation = 'relu')) 
model.add (Dense(500, activation = 'relu'))
model.add (Dense(128, activation = 'relu'))
model.add (Dense(2, activation = 'softmax')) 
model.compile (loss = 'binary_crossentropy', optimizer = 'adam')

y_train = np.asarray(y_train).astype(np.float32)
y_test = np.asarray(y_test).astype(np.float32)

model.fit(X_train, y_train, batch_size = 12, epochs=35)
y_pred = model.predict(X_test)

y_pred=np.argmax(y_pred, axis=1)
y_test=np.argmax(y_test, axis=1)

# Q4) Accuracy

print('Accuracy Score (%) ', accuracy_score(y_test, y_pred)*100) 

# Q5) Actual Data Points

y_pred=pd.DataFrame (y_pred, columns = ['chd']) #changing numpy array to dataframe to allow for concatenation below
y_test=pd.DataFrame (y_test, columns = ['chd']) 

sbp_0 = []
sbp_1 = []
tob_0 = []
tob_1 = []

result = pd.concat([X_test, y_test], axis=1)  #For actual data points

w = result.loc[result['chd']==0, 'sbp']
sbp_0.append(w)

w = result.loc[result['chd']==1, 'sbp']
sbp_1.append(w)

w = result.loc[result['chd']==0, 'tobacco']
tob_0.append(w)

w = result.loc[result['chd']==1, 'tobacco']
tob_1.append(w)

plt.scatter (tob_0, sbp_0, color='g',  label = 'chd = 0' )
plt.scatter (tob_1, sbp_1, color='b', label = 'chd = 1' )
plt.title ('Standardized Plot of sbp vs. tobacco: Actual Data Points')
plt.xlabel ('tobacco')
plt.ylabel ('sbp')
plt.legend()

# Q6) Predicted Data Points

sbp_0_pred = []
sbp_1_pred = []
tob_0_pred = []
tob_1_pred = []

result = pd.concat([X_test, y_pred], axis=1)  #For predicted data points

w = result.loc[result['chd']==0, 'sbp']
sbp_0_pred.append(w)

w = result.loc[result['chd']==1, 'sbp']
sbp_1_pred.append(w)

w = result.loc[result['chd']==0, 'tobacco']
tob_0_pred.append(w)

w = result.loc[result['chd']==1, 'tobacco']
tob_1_pred.append(w)

plt.figure()
plt.scatter (tob_0_pred, sbp_0_pred, color='g',  label = 'chd = 0' )
plt.scatter (tob_1_pred, sbp_1_pred, color='b', label = 'chd = 1' )
plt.title ('Standardized Plot of sbp vs. tobacco: Predicted Data Points')
plt.xlabel ('tobacco')
plt.ylabel ('sbp')
plt.legend()

########## i) Calculating Principal Components + Performing KMeans with PCAs ##########

data = pd.read_csv ('heart.csv')

# One-Hot Encoding of 'famhist' 
OHE = pd.get_dummies(data['famhist'])
data = data.drop('famhist', axis=1)
data = data.join(OHE)

X = data.drop(columns = 'chd')
y = data['chd']

#Q7: How many PCs explain more than 90% of variation?
pca = PCA(n_components=11)
principalComponents = pca.fit_transform(X)

pca_explainedratio = pca.explained_variance_ratio_
print('Explained variation per principal component: \n{}'.format(pca_explainedratio))
print('\nPercentage first component = {}'.format(pca_explainedratio[0]*100))
print ('Therefore, one PC (the first component) explains more than 90% of variation.')

# Q8: How much variation is explained by first two PCs?

pca_explainedratio = pca.explained_variance_ratio_[0]+pca.explained_variance_ratio_[1]
print('\nExplained variation by first two PCs: {}'.format(pca_explainedratio*100))
print ('Therefore, 95.97% of the variance is explained by the first two PCs.\n')

## K-Means clustering using PCs that explain 90% variation ##

principal_df = pd.DataFrame (data = principalComponents, columns = ['PC1', 'PC2', 'PC3', 'PC4', 'PC5', 'PC6', 'PC7', 'PC8', 'PC9', 'PC10', 'PC11'])

X_train, X_test, y_train, y_test = train_test_split (X,y, test_size = 0.5, random_state=45)
scaler = StandardScaler()
scaler.fit(X_train)
X_train = scaler.transform (X_train)
X_test = scaler.transform (X_test)

pca = PCA(.90) #min number of PCs will be chosen such that 90% of variance is retained
pca.fit(X_train)
X_train = pca.transform(X_train)
X_test = pca.transform (X_test)

km = KMeans (n_clusters = 2, random_state=42)
km.fit(X_train, y_train)
y_pred = km.predict(X_test)

#Q9. Accuracy
print('Accuracy Score (%) ', accuracy_score(y_test, y_pred)*100) 

#Q10. PC1 vs PC2. Actual data points

y_test=pd.DataFrame (y_test, columns = ['chd']) 
df_final = pd.concat([principal_df, y_test], axis=1)

fig = plt.figure()
ax = fig.add_subplot (1,1,1)
ax.set_xlabel ('PC 1')
ax.set_ylabel ('PC 2')
ax.set_title ('PC 1 vs. PC 2: Actual Data')

targets = [0,1]
colors = ['r', 'b']
for target, color in zip(targets, colors):
  indicesToKeep = df_final['chd']== target
  ax. scatter (df_final.loc[indicesToKeep, 'PC1'], df_final.loc[indicesToKeep, 'PC2'], c=color)
  ax.legend(targets)


#Q11 ) PC1 vs PC2: Predicted data points

y_pred=pd.DataFrame (y_pred, columns = ['chd']) 
df_final2 = pd.concat([principal_df, y_pred], axis=1)

fig = plt.figure()
ax = fig.add_subplot (1,1,1)
ax.set_xlabel ('PC 1')
ax.set_ylabel ('PC 2')
ax.set_title ('PC 1 vs. PC 2: Predicted Data')

targets = [0,1]
colors = ['r', 'b']
for target, color in zip(targets, colors):
  indicesToKeep = df_final2['chd']== target
  ax. scatter (df_final2.loc[indicesToKeep, 'PC1'], df_final2.loc[indicesToKeep, 'PC2'], c=color)
  ax.legend(targets)

########## ii) Classification using NN with PCs that explain 90% variation ##########

data = pd.read_csv ('heart.csv')

# One-Hot Encoding of 'famhist' 
OHE = pd.get_dummies(data['famhist'])
data = data.drop('famhist', axis=1)
data = data.join(OHE)

X = data.drop(columns = 'chd')
y = data['chd']

y = tf.one_hot(y, 2) 
y = pd.DataFrame (y, columns=['chd_OHE0', 'chd_OHE1']) #changing y back to a dataframe to allow for train/test split

pca = PCA(n_components=11)
principalComponents = pca.fit_transform(X)
principal_df = pd.DataFrame (data = principalComponents, columns = ['PC1', 'PC2', 'PC3', 'PC4', 'PC5', 'PC6', 'PC7', 'PC8', 'PC9', 'PC10', 'PC11'])

X_train, X_test, y_train, y_test = train_test_split (X,y, test_size = 0.5, random_state=45)
scaler = StandardScaler()
scaler.fit(X_train)
X_train = scaler.transform (X_train)
X_test = scaler.transform (X_test)

pca = PCA(.90) #min number of PCs will be chosen such that 90% of variance is retained
pca.fit(X_train)
X_train = pca.transform(X_train)
X_test = pca.transform (X_test)

model = Sequential()
model.add(Dense(500, input_shape=(X_train.shape[1],), activation = 'relu')) 
model.add (Dense(500, activation = 'relu'))
model.add (Dense(128, activation = 'relu'))
model.add (Dense(2, activation = 'softmax')) 
model.compile (loss = 'binary_crossentropy', optimizer = 'adam')

y_train = np.asarray(y_train).astype(np.float32)
y_test = np.asarray(y_test).astype(np.float32)

model.fit(X_train, y_train, batch_size = 12, epochs=35)
y_pred = model.predict(X_test)

y_pred=np.argmax(y_pred, axis=1)
y_test=np.argmax(y_test, axis=1)

# Q12) Accuracy

print('Accuracy Score (%) ', accuracy_score(y_test, y_pred)*100) 


# Q13 ) PC1 vs PC2: Actual data points

y_test=pd.DataFrame (y_test, columns = ['chd']) 
df_final = pd.concat([principal_df, y_test], axis=1)

fig = plt.figure()
ax = fig.add_subplot (1,1,1)
ax.set_xlabel ('PC 1')
ax.set_ylabel ('PC 2')
ax.set_title ('PC 1 vs. PC 2: Actual Data')

targets = [0,1]
colors = ['r', 'b']
for target, color in zip(targets, colors):
  indicesToKeep = df_final['chd']== target
  ax. scatter (df_final.loc[indicesToKeep, 'PC1'], df_final.loc[indicesToKeep, 'PC2'], c=color)
  ax.legend(targets)

# Q14 ) PC1 vs PC2: Predicted data points

y_pred=pd.DataFrame (y_pred, columns = ['chd']) 
df_final2 = pd.concat([principal_df, y_pred], axis=1)

fig = plt.figure()
ax = fig.add_subplot (1,1,1)
ax.set_xlabel ('PC 1')
ax.set_ylabel ('PC 2')
ax.set_title ('PC 1 vs. PC 2: Predicted Data')

targets = [0,1]
colors = ['r', 'b']
for target, color in zip(targets, colors):
  indicesToKeep = df_final2['chd']== target
  ax. scatter (df_final2.loc[indicesToKeep, 'PC1'], df_final2.loc[indicesToKeep, 'PC2'], c=color)
  ax.legend(targets)

################## ADDITIONAL ##################

from sklearn.linear_model import LogisticRegression

df = pd.read_csv ('heart.csv')

# One-Hot Encoding of 'famhist' 
OHE = pd.get_dummies(df['famhist'])
df = df.drop('famhist', axis=1)
df = df.join(OHE)

#Split DataFrame into Feature and Target
df_x = df.iloc[:, np.r_[1:9, 10,11]].copy()
df_y = df.iloc[:,9].copy()
    
X_train, X_test, y_train, y_test = train_test_split (df_x, df_y, test_size = 0.5, random_state=45)

sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

logreg = LogisticRegression()
logreg.fit(X_train,y_train)
y_pred=logreg.predict(X_test)

print('Accuracy Score for raw data (%) ', accuracy_score(y_test, y_pred)*100) 


################################

df = pd.read_csv ('heart.csv')

# One-Hot Encoding of 'famhist' 
OHE = pd.get_dummies(df['famhist'])
df = df.drop('famhist', axis=1)
df = df.join(OHE)

#Split DataFrame into Feature and Target
df_x = df.iloc[:, np.r_[1:9, 10,11]].copy()
df_y = df.iloc[:,9].copy()

pca = PCA(n_components=10)
principalComponents = pca.fit_transform(df_x)
principal_df = pd.DataFrame (data = principalComponents, columns = ['PC1', 'PC2', 'PC3', 'PC4', 'PC5', 'PC6', 'PC7', 'PC8', 'PC9', 'PC10'])

X_train, X_test, y_train, y_test = train_test_split (df_x, df_y, test_size = 0.5, random_state=45)
scaler = StandardScaler()
scaler.fit(X_train)
X_train = scaler.transform (X_train)
X_test = scaler.transform (X_test)

pca = PCA(.90) #min number of PCs will be chosen such that 90% of variance is retained
pca.fit(X_train)
X_train = pca.transform(X_train)
X_test = pca.transform (X_test)

logreg = LogisticRegression()
logreg.fit(X_train,y_train)
y_pred=logreg.predict(X_test)

print('Accuracy Score for PCs that explain 90% variation (%) ', accuracy_score(y_test, y_pred)*100)