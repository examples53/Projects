#Mahima Hasmani

# Multilayer neural network implementation

import numpy as np
import matplotlib.pyplot as plt
from sklearn.utils import shuffle
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense

#Q1. Data Synthesis

def y_function (x):
    return 0.2*x**4 + 2*x**3 + 0.1*x**2 + 10

x = np.linspace (-1, 1, num=30000)
y = y_function (x)

plt.plot (x,y)
plt.xlabel ('x vales')
plt.ylabel ('y values')
plt.title ('Initial Plot of Function')

#Q2. Shuffling 

def get_dataset (data, flag_):
    if flag_ == 0:
        data = shuffle(data, random_state=42) #random state provides reproducible result
    return data

#Q3. Data Splitting

#Note: the function only takes in the train and validate ratios as inputs because the difference between the two 
#automatically denotes the size of the test data

def data_split (datax, datay, train_ratio, validate_ratio):   
    xtrain, xvalidate, xtest = np.split(datax, [int(train_ratio*len(datax)), int(validate_ratio*len(datax))]) 
    ytrain, yvalidate, ytest = np.split(datay, [int(train_ratio*len(datay)), int(validate_ratio*len(datay))]) 
    return xtrain, xvalidate, xtest, ytrain, yvalidate, ytest

#Q4. Scaling

x = x.reshape(-1,1)
y = y.reshape(-1,1)

def scaling (var):
    scaler = MinMaxScaler()
    return scaler.fit_transform(var)


#Q5. Calculation of Various Numerical Metrics

def metrics (y_true, y_predict):
    print('MAE: ', mean_absolute_error(y_true, y_predict))
    print ('MSE: ', mean_squared_error(y_true, y_predict))
    print ('RMSE: ', mean_squared_error(y_true, y_predict, squared=False))
    print ('r^2 score: ', r2_score (y_true, y_predict))

def struct1 (actfunc, fig1title, fig2title):
  model = Sequential()
  model.add (Dense(12, input_dim=1, activation = actfunc))
  model.add (Dense(8, activation = actfunc))
  model.add (Dense(4, activation = actfunc))
  model.add (Dense(1, activation = 'linear')) #since this is a function approximation problem, a 'linear' activation function is used in the output layer

  model.compile (loss = 'mse', optimizer = 'adam')
  model.fit(xtrain, ytrain, batch_size = 12, epochs=20, validation_data = (xvalidate, yvalidate))
  y_pred = model.predict(xtest)

  plt.figure
  plt.scatter(xtest, ytest)
  plt.title (fig1title)
  plt.show()

  plt.figure
  plt.scatter(xtest, y_pred)
  plt.title (fig2title)
  plt.show()

  metrics (ytest, y_pred)

  
def struct2 (actfunc, fig1title, fig2title):
  model = Sequential()
  model.add (Dense(24, input_dim=1, activation = actfunc))
  model.add (Dense(1, activation = 'linear')) #since this is a function approximation problem, a 'linear' activation function is used in the output layer

  model.compile (loss = 'mse', optimizer = 'adam')
  model.fit(xtrain, ytrain, batch_size = 12, epochs=20, validation_data = (xvalidate, yvalidate))
  y_pred = model.predict(xtest)

  plt.figure
  plt.scatter(xtest, ytest)
  plt.title (fig1title)
  plt.show()

  plt.figure
  plt.scatter(xtest, y_pred)
  plt.title (fig2title)
  plt.show()

  metrics (ytest, y_pred)

ReLU_ = 'relu'
tanh_ = 'tanh'


#Shuffle the data
x = get_dataset (x, 0)
y = get_dataset (y, 0)


#Split data into ratios
xtrain, xvalidate, xtest, ytrain, yvalidate, ytest = data_split (x, y, 0.3, 0.5) #to split the xdata

############ CASE IMPLEMENTATIONS ############

#CASE 1: shuffled and unscaled data; structure 1 w/ Relu
title_1 = 'Case 1: Actual Test Data'
title_2 = 'Case 1: Predicted Data'
struct1(ReLU_, title_1, title_2)

#CASE 2: shuffled and unscaled data; structure 2 w/ Relu

x = np.linspace (-1, 1, num=30000)
y = y_function (x)
x = x.reshape(-1,1)
y = y.reshape(-1,1)


#Shuffle the data
x = get_dataset (x, 0)
y = get_dataset (y, 0)


#Split data into ratios
xtrain, xvalidate, xtest, ytrain, yvalidate, ytest = data_split (x, y, 0.3, 0.5) #to split the xdata

title_1 = 'Case 2: Actual Test Data'
title_2 = 'Case 2: Predicted Data'
struct2(ReLU_, title_1, title_2)

#CASE 3: shuffled and unscaled data; structure 1 w/ tanh

x = np.linspace (-1, 1, num=30000)
y = y_function (x)
x = x.reshape(-1,1)
y = y.reshape(-1,1)

#Shuffle the data
x = get_dataset (x, 0)
y = get_dataset (y, 0)


#Split data into ratios
xtrain, xvalidate, xtest, ytrain, yvalidate, ytest = data_split (x, y, 0.3, 0.5) #to split the xdata

title_1 = 'Case 3: Actual Test Data'
title_2 = 'Case 3: Predicted Data'
struct1(tanh_, title_1, title_2)

#CASE 4: shuffled and scaled data; structure 1 w/ Relu

x = np.linspace (-1, 1, num=30000)
y = y_function (x)
x = x.reshape(-1,1)
y = y.reshape(-1,1)

#Shuffle the data
x = get_dataset (x, 0)
y = get_dataset (y, 0)

#Split data into ratios
xtrain, xvalidate, xtest, ytrain, yvalidate, ytest = data_split (x, y, 0.3, 0.5) #to split the xdata

#Scale the data
xtrain =scaling(xtrain)
ytrain =scaling(ytrain)
xvalidate =scaling(xvalidate)
xvalidate =scaling(xvalidate)
xtest =scaling(xtest)
ytest =scaling(ytest)

title_1 = 'Case 4: Actual Test Data'
title_2 = 'Case 4: Predicted Data'
struct1(ReLU_, title_1, title_2)

#CASE 5: shuffled and scaled data; structure 1 w/ tanh

x = np.linspace (-1, 1, num=30000)
y = y_function (x)
x = x.reshape(-1,1)
y = y.reshape(-1,1)

#Shuffle the data
x = get_dataset (x, 0)
y = get_dataset (y, 0)

#Split data into ratios
xtrain, xvalidate, xtest, ytrain, yvalidate, ytest = data_split (x, y, 0.3, 0.5) #to split the xdata

#Scale the data
xtrain =scaling(xtrain)
ytrain =scaling(ytrain)
xvalidate =scaling(xvalidate)
xvalidate =scaling(xvalidate)
xtest =scaling(xtest)
ytest =scaling(ytest)

title_1 = 'Case 5: Actual Test Data'
title_2 = 'Case 5: Predicted Data'
struct1(tanh_, title_1, title_2)

x = np.linspace (-1, 1, num=30000)
y = y_function (x)
x = x.reshape(-1,1)
y = y.reshape(-1,1)

#Split data into ratios
xtrain, xvalidate, xtest, ytrain, yvalidate, ytest = data_split (x, y, 0.3, 0.5) #to split the xdata

#CASE 6: Unshuffled and unscaled data; structure 1 w/ Relu
title_1 = 'Case 6: Actual Test Data'
title_2 = 'Case 6: Predicted Data'
struct1(ReLU_, title_1, title_2)

#CASE 7: Unshuffled and unscaled data; structure 2 w/ Relu

x = np.linspace (-1, 1, num=30000)
y = y_function (x)
x = x.reshape(-1,1)
y = y.reshape(-1,1)

#Split data into ratios
xtrain, xvalidate, xtest, ytrain, yvalidate, ytest = data_split (x, y, 0.3, 0.5) #to split the xdata

title_1 = 'Case 7: Actual Test Data'
title_2 = 'Case 7: Predicted Data'
struct2(ReLU_, title_1, title_2)

#CASE 8: Unshuffled and unscaled data; structure 1 w/ tanh

x = np.linspace (-1, 1, num=30000)
y = y_function (x)
x = x.reshape(-1,1)
y = y.reshape(-1,1)


#Split data into ratios
xtrain, xvalidate, xtest, ytrain, yvalidate, ytest = data_split (x, y, 0.3, 0.5) #to split the xdata

title_1 = 'Case 8: Actual Test Data'
title_2 = 'Case 8: Predicted Data'
struct1(tanh_, title_1, title_2)

#CASE 9: Unshuffled and scaled data; structure 1 w/ Relu

x = np.linspace (-1, 1, num=30000)
y = y_function (x)
x = x.reshape(-1,1)
y = y.reshape(-1,1)

#Split data into ratios
xtrain, xvalidate, xtest, ytrain, yvalidate, ytest = data_split (x, y, 0.3, 0.5) #to split the xdata

#Scale the data
xtrain =scaling(xtrain)
ytrain =scaling(ytrain)
xvalidate =scaling(xvalidate)
xvalidate =scaling(xvalidate)
xtest =scaling(xtest)
ytest =scaling(ytest)

title_1 = 'Case 9: Actual Test Data'
title_2 = 'Case 9: Predicted Data'
struct1(ReLU_, title_1, title_2)

#CASE 10: unshuffled and scaled data; structure 1 w/ tanh

x = np.linspace (-1, 1, num=30000)
y = y_function (x)
x = x.reshape(-1,1)
y = y.reshape(-1,1)

#Split data into ratios
xtrain, xvalidate, xtest, ytrain, yvalidate, ytest = data_split (x, y, 0.3, 0.5) #to split the xdata

#Scale the data
xtrain =scaling(xtrain)
ytrain =scaling(ytrain)
xvalidate =scaling(xvalidate)
xvalidate =scaling(xvalidate)
xtest =scaling(xtest)
ytest =scaling(ytest)

title_1 = 'Case 10: Actual Test Data'
title_2 = 'Case 10: Predicted Data'
struct1(tanh_, title_1, title_2)