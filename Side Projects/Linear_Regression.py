# Mahima Hasmani

# This file explores the properties of *univariate linear regression* by modeling the relationship between the students' midterm grades and final exam grades.
# Data preparation and exploratory data analysis steps are implemented.
# A gradient descent algorithm is implemented from scratch through different cases to minimize the model's cost function (error).
# Applications: analyzing continuous linear trends, estimation and forecasting.

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import StandardScaler

student_marks = pd.read_csv('student_marks.csv')

#Data prepration and EDA

mean_md = student_marks['Midterm mark'].mean()
std_md = student_marks['Midterm mark'].std()
# print('Midterm mean = ', mean_md) 
# print('Midterm std = ', std_md) 


mean_f = student_marks['Final mark'].mean()
std_f = student_marks['Final mark'].std()
# print('Final exam mean = ', mean_f) 
# print('Final exam  std = ', std_f) 
# print('\n')

mid_mark = student_marks ['Midterm mark']
mid_mark = mid_mark.values.reshape(-1,1)

fin_mark = student_marks ['Final mark']
fin_mark = fin_mark.values.reshape(-1,1)

"""# Unstandardized: Initial Regression Line with alpha = 0.0001"""

plt.figure()
plt.scatter (mid_mark, fin_mark)

a = 0.0001
b=0
m=-0.5

y= m*mid_mark + b
plt.plot(mid_mark, y)
plt.title ('Regression Line with m=-0.5, b=0, a=0.1 (Untandardized) ')
plt.xlabel('Midterm mark')
plt.ylabel ('Final mark')

"""# Unstandardized: 100 times update with alpha = 0.0001"""

def PartialDer_m (x, y,m,b):
    return (2/len(x))*np.sum((-x*(y-((m*x +b)))))
    
def PartialDer_b (x,y,m,b):
    return (2/len(x))*np.sum((-(y-((m*x) +b))))

def update (x, y, m, b, alpha):
    m_new = m - (alpha * PartialDer_m(x, y, m, b))
    b_new = b - (alpha * PartialDer_b(x, y, m, b))
    return m_new, b_new

def errorFunc (x,y,m,b):
    return (1/len(x)) * np.sum(np.square(y - (m*x + b)))

    
iter_arr = [] #empty list to be used to store iterations
error_arr = [] #empty list to be used to store the error per iteration

for i in range (1,101):
    m_new, b_new = update(mid_mark, fin_mark, m, b, a)
    error_arr.append(errorFunc (mid_mark, fin_mark, m, b)) 
    iter_arr.append(i)
    m = m_new
    b = b_new

print('Final m_value: ', m)
print ('Final b_value: ', b)
print('Sum of Errors: ', sum(error_arr))
print ('Last Value of Error in Array: ', error_arr[-1])
print('\n')
print(error_arr)

plt.figure()
plt.scatter (mid_mark, fin_mark)

y= m*mid_mark + b
plt.plot(mid_mark, y)
plt.suptitle('100x updated b and m (Unstandardized)');      
plt.xlabel('Midterm mark')
plt.ylabel ('Final mark')

plt.figure()
plt.scatter(iter_arr, error_arr)
plt.suptitle('Error at each iteration until 100 (Unstandardized)');      
plt.xlabel('Iteration Number')
plt.ylabel ('Error')

"""# Unstandardized: 2000 times update with alpha = 0.0001"""

iter_arr = [] #empty list to be used to store iterations
error_arr = [] #empty list to be used to store the error per iteration

for i in range (1,2001):
    m_new, b_new = update(mid_mark, fin_mark, m, b, a)
    error_arr.append(errorFunc (mid_mark, fin_mark, m, b)) 
    iter_arr.append(i)
    m = m_new
    b = b_new
print('Final m_value: ', m)
print ('Final b_value: ', b)
print('Sum of Errors: ', sum(error_arr))
print ('Last Value of Error in Array: ', error_arr[-1])
print('\n')
print(error_arr)



plt.figure()
plt.scatter (mid_mark, fin_mark)

y= m*mid_mark + b
plt.plot(mid_mark, y)
plt.title ('2000x updated b and m (Unstandardized)')
plt.xlabel('Midterm mark')
plt.ylabel ('Final mark')

plt.figure()
plt.scatter(iter_arr, error_arr)
plt.suptitle('Error at each iteration until 2000 (Unstandardized)')    
plt.xlabel('Iteration Number')
plt.ylabel ('Error')

"""# Standardized: Initial Regression Line """

def Standardization (df, mean, std):
    for i in range (len(df)):
        df[i] = (df[i]-mean)/std

mid_mark = student_marks ['Midterm mark']
mid_mark = mid_mark.values.reshape(-1,1)

fin_mark = student_marks ['Final mark']
fin_mark = fin_mark.values.reshape(-1,1)

Standardization (mid_mark, mean_md, std_md)
Standardization (fin_mark, mean_f, std_f)

plt.figure()
plt.scatter (mid_mark, fin_mark)

a = 0.0001
#a= 0.1
b=0
m=-0.5

y= m*mid_mark + b
plt.plot(mid_mark, y)
plt.title ('Regression Line with m=-0.5, b=0, a=0.1 (Standardized) ')
plt.xlabel('Midterm mark')
plt.ylabel ('Final mark')

"""# Standardized: 100 times update with alpha = 0.0001"""

iter_arr = [] #empty list to be used to store iterations
error_arr = [] #empty list to be used to store the error per iteration

for i in range (1,101):
    m_new, b_new = update(mid_mark, fin_mark, m, b, a)
    error_arr.append(errorFunc (mid_mark, fin_mark, m, b)) 
    iter_arr.append(i)
    m = m_new
    b = b_new

print('Final m_value: ', m)
print ('Final b_value: ', b)
print('Sum of Errorzs: ', sum(error_arr))
print ('Last Value of Error in Array: ', error_arr[-1])
print('\n')
print(error_arr)

plt.figure()
plt.scatter (mid_mark, fin_mark)

y= m*mid_mark + b
plt.plot(mid_mark, y)
plt.suptitle('100x updated b and m (Standardized)');      
plt.xlabel('Midterm mark')
plt.ylabel ('Final mark')

plt.figure()
plt.scatter(iter_arr, error_arr)
plt.suptitle('Error at each iteration until 100 (Standardized)');      
plt.xlabel('Iteration Number')
plt.ylabel ('Error')

"""# Standardized: 2000 times update with alpha = 0.0001"""

iter_arr = [] #empty list to be used to store iterations
error_arr = [] #empty list to be used to store the error per iteration

for i in range (1,2001):
    m_new, b_new = update(mid_mark, fin_mark, m, b, a)
    error_arr.append(errorFunc (mid_mark, fin_mark, m, b)) 
    iter_arr.append(i)
    m = m_new
    b = b_new
print('Final m_value: ', m)
print ('Final b_value: ', b)
print('Sum of Errors: ', sum(error_arr))
print ('Last Value of Error in Array: ', error_arr[-1])
print('\n')
print(error_arr)



plt.figure()
plt.scatter (mid_mark, fin_mark)

y= m*mid_mark + b
plt.plot(mid_mark, y)
plt.title ('2000x updated b and m (Standardized)')
plt.xlabel('Midterm mark')
plt.ylabel ('Final mark')

plt.figure()
plt.scatter(iter_arr, error_arr)
plt.suptitle('Error at each iteration until 2000 (Standardized)', y=1.0004);      
plt.xlabel('Iteration Number')
plt.ylabel ('Error')

"""# Standardization with Scikit-Learn: Initial Regression Line"""

mid_mark = student_marks ['Midterm mark']
mid_mark = mid_mark.values.reshape(-1,1)

fin_mark = student_marks ['Final mark']
fin_mark = fin_mark.values.reshape(-1,1)

mid_mark = StandardScaler().fit_transform (mid_mark)
fin_mark = StandardScaler().fit_transform (fin_mark)

plt.figure()
plt.scatter (mid_mark, fin_mark)

a = 0.0001
b=0
m=-0.5

y= m*mid_mark + b
plt.plot(mid_mark, y)
plt.title ('Regression Line with m=-0.5, b=0, a=0.1 (Standardized) ')
plt.xlabel('Midterm mark')
plt.ylabel ('Final mark')

"""# Standardization with Scikit-Learn: 100 times update with alpha = 0.0001"""

mid_mark = student_marks ['Midterm mark']
mid_mark = mid_mark.values.reshape(-1,1)

fin_mark = student_marks ['Final mark']
fin_mark = fin_mark.values.reshape(-1,1)

mid_mark = StandardScaler().fit_transform (mid_mark)
fin_mark = StandardScaler().fit_transform (fin_mark)

iter_arr = [] #empty list to be used to store iterations
error_arr = [] #empty list to be used to store the error per iteration

for i in range (1,101):
    m_new, b_new = update(mid_mark, fin_mark, m, b, a)
    error_arr.append(errorFunc (mid_mark, fin_mark, m, b)) 
    iter_arr.append(i)
    m = m_new
    b = b_new

print('Final m_value: ', m)
print ('Final b_value: ', b)
print('Sum of Errorzs: ', sum(error_arr))
print ('Last Value of Error in Array: ', error_arr[-1])
print('\n')
print(error_arr)

plt.figure()
plt.scatter (mid_mark, fin_mark)

y= m*mid_mark + b
plt.plot(mid_mark, y)
plt.suptitle('100x updated b and m (Standardized)');      
plt.xlabel('Midterm mark')
plt.ylabel ('Final mark')

plt.figure()
plt.scatter(iter_arr, error_arr)
plt.suptitle('Error at each iteration until 100 (Standardized)');      
plt.xlabel('Iteration Number')
plt.ylabel ('Error')

"""# Standardization with Scikit-Learn: 2000 times update with alpha = 0.0001"""

mid_mark = student_marks ['Midterm mark']
mid_mark = mid_mark.values.reshape(-1,1)

fin_mark = student_marks ['Final mark']
fin_mark = fin_mark.values.reshape(-1,1)

mid_mark = StandardScaler().fit_transform (mid_mark)
fin_mark = StandardScaler().fit_transform (fin_mark)

iter_arr = [] #empty list to be used to store iterations
error_arr = [] #empty list to be used to store the error per iteration

for i in range (1,2001):
    m_new, b_new = update(mid_mark, fin_mark, m, b, a)
    error_arr.append(errorFunc (mid_mark, fin_mark, m, b)) 
    iter_arr.append(i)
    m = m_new
    b = b_new

print('Final m_value: ', m)
print ('Final b_value: ', b)
print('Sum of Errorzs: ', sum(error_arr))
print ('Last Value of Error in Array: ', error_arr[-1])
print('\n')
print(error_arr)

plt.figure()
plt.scatter (mid_mark, fin_mark)

y= m*mid_mark + b
plt.plot(mid_mark, y)
plt.suptitle('2000x updated b and m (Standardized)');      
plt.xlabel('Midterm mark')
plt.ylabel ('Final mark')

plt.figure()
plt.scatter(iter_arr, error_arr)
plt.suptitle('Error at each iteration until 2000 (Standardized)');      
plt.xlabel('Iteration Number')
plt.ylabel ('Error')

"""# Unstandardized: Initial Regression Line with alpha = 0.1"""

student_marks = pd.read_csv('student_marks.csv')

#Data prepration and EDA

mid_mark = student_marks ['Midterm mark']
mid_mark = mid_mark.values.reshape(-1,1)

fin_mark = student_marks ['Final mark']
fin_mark = fin_mark.values.reshape(-1,1)

plt.figure()
plt.scatter (mid_mark, fin_mark)

#a = 0.0001
a= 0.1
b=0
m=-0.5

y= m*mid_mark + b
plt.plot(mid_mark, y)
plt.title ('Regression Line with m=-0.5, b=0, a=0.1 (Untandardized) ')
plt.xlabel('Midterm mark')
plt.ylabel ('Final mark')

"""# Unstandardized: 100 times update with alpha = 0.1"""

iter_arr = [] #empty list to be used to store iterations
error_arr = [] #empty list to be used to store the error per iteration

for i in range (1,101):
    m_new, b_new = update(mid_mark, fin_mark, m, b, a)
    error_arr.append(errorFunc (mid_mark, fin_mark, m, b)) 
    iter_arr.append(i)
    m = m_new
    b = b_new

print('Final m_value: ', m)
print ('Final b_value: ', b)
print('Sum of Errors: ', sum(error_arr))
print ('Last Value of Error in Array: ', error_arr[-1])
print('\n')
print(error_arr)

plt.figure()
plt.scatter (mid_mark, fin_mark)

y= m*mid_mark + b
plt.plot(mid_mark, y)
plt.suptitle('Case 6: 100x updated b and m (Unstandardized)');      
plt.xlabel('Midterm mark')
plt.ylabel ('Final mark')

plt.figure()
plt.scatter(iter_arr, error_arr)
plt.suptitle('Error at each iteration until 100 (Unstandardized)');      
plt.xlabel('Iteration Number')
plt.ylabel ('Error')

"""# Unstandardized: 2000 times update with alpha = 0.1"""

iter_arr = [] #empty list to be used to store iterations
error_arr = [] #empty list to be used to store the error per iteration

for i in range (1,2001):
    m_new, b_new = update(mid_mark, fin_mark, m, b, a)
    error_arr.append(errorFunc (mid_mark, fin_mark, m, b)) 
    iter_arr.append(i)
    m = m_new
    b = b_new
print('Final m_value: ', m)
print ('Final b_value: ', b)
print('Sum of Errors: ', sum(error_arr))
print ('Last Value of Error in Array: ', error_arr[-1])
print('\n')
print(error_arr)


plt.figure()
plt.scatter (mid_mark, fin_mark)

y= m*mid_mark + b
plt.plot(mid_mark, y)
plt.title ('Case 7: 2000x updated b and m (Unstandardized)')
plt.xlabel('Midterm mark')
plt.ylabel ('Final mark')

plt.figure()
plt.scatter(iter_arr, error_arr)
plt.suptitle('Error at each iteration until 2000 (Unstandardized)')    
plt.xlabel('Iteration Number')
plt.ylabel ('Error')

"""# Standardized: Initial Regression Line with alpha = 0.1"""

Standardization (mid_mark, mean_md, std_md)
Standardization (fin_mark, mean_f, std_f)

plt.figure()
plt.scatter (mid_mark, fin_mark)

a = 0.1
b=0
m=-0.5

y= m*mid_mark + b
plt.plot(mid_mark, y)
plt.title ('Regression Line with m=-0.5, b=0, a=0.1 (Standardized) ')
plt.xlabel('Midterm mark')
plt.ylabel ('Final mark')

"""# Standardized: 100 times update with alpha = 0.1"""

def errorFunc (x,y,m,b):
    return (1/len(x)) * np.sum(np.square(y - (m*x + b)))

iter_arr = [] #empty list to be used to store iterations
error_arr = [] #empty list to be used to store the error per iteration

for i in range (1,101):
    m_new, b_new = update(mid_mark, fin_mark, m, b, a)
    error_arr.append(errorFunc (mid_mark, fin_mark, m, b)) 
    iter_arr.append(i)
    m = m_new
    b = b_new

print('Final m_value: ', m)
print ('Final b_value: ', b)
print('Sum of Errors: ', sum(error_arr))
print ('Last Value of Error in Array: ', error_arr[-1])
print('\n')
print(error_arr)

plt.figure()
plt.scatter (mid_mark, fin_mark)

y= m*mid_mark + b
plt.plot(mid_mark, y)
plt.suptitle('Case 8: 100x updated b and m (Standardized)');      
plt.xlabel('Midterm mark')
plt.ylabel ('Final mark')

plt.figure()
plt.scatter(iter_arr, error_arr)
plt.suptitle('Error at each iteration until 100 (Standardized)');      
plt.xlabel('Iteration Number')
plt.ylabel ('Error')

"""# Standardized: 2000 times update with alpha = 0.1"""

def errorFunc (x,y,m,b):
    return (1/len(x)) * np.sum(np.square(y - (m*x + b)))

iter_arr = [] #empty list to be used to store iterations
error_arr = [] #empty list to be used to store the error per iteration

for i in range (1,2001):
    m_new, b_new = update(mid_mark, fin_mark, m, b, a)
    error_arr.append(errorFunc (mid_mark, fin_mark, m, b)) 
    iter_arr.append(i)
    m = m_new
    b = b_new

print('Final m_value: ', m)
print ('Final b_value: ', b)
print('Sum of Errors: ', sum(error_arr))
print ('Last Value of Error in Array: ', error_arr[-1])
print('\n')
print(error_arr)


plt.figure()
plt.scatter (mid_mark, fin_mark)

y= m*mid_mark + b
plt.plot(mid_mark, y)
plt.suptitle('Case 9: 2000x updated b and m (Standardized)');      
plt.xlabel('Midterm mark')
plt.ylabel ('Final mark')

plt.figure()
plt.scatter(iter_arr, error_arr)
plt.suptitle('Error at each iteration until 2000 (Standardized)');      
plt.xlabel('Iteration Number')
plt.ylabel ('Error')







