#Mahima Hasmani

# Implemented logistic regression from scratch as well as using the SKlearn library.
# Explored different cases by varying the learning rate, epochs and batch sizes.

import pandas as pd
import matplotlib.pyplot as plt
import random
import seaborn as sn
import numpy as np
import io
from google.colab import files
from sklearn.metrics import accuracy_score

uploaded = files.upload()
df = pd.read_csv(io.BytesIO(uploaded['heart.csv']))

#Important Functions

def Standardization (df):  
  mean = np.mean(df)
  std = np.std(df)
  return [(i-mean)/std for i in df]

def hypothesis (weights, ft):
  z=np.sum(weights*ft)
  return 1/(1 + np.exp(-1*z))

def CostFunc (weights, x, y): 
  err = 0
  for i in range(len(x)):
        err += y[i] * np.log(hypothesis(weights, x[i])) + (1-y[i])* (np.log(1- hypothesis (weights, x[i])))
  return -1*err / len(x)

def Update (x,y,weights, a):
  thetas_new = []
  theta_len = len(weights)
  x_len = len(x)
  for i in range(theta_len):
    grad =0 
    for j in range (x_len):
      grad += x [j][i] * (y [j] - hypothesis (weights, x[j]))
    grad = grad / len (x)
    thetas_new.append(weights[i] + a*grad)
  return thetas_new


###################### EDA ######################

x = df['chd'].value_counts(normalize=True)
print ('The percentage of each class is: \n', x*100, '\n')

print('The categorical variable(s) in the list of features is: ', df.select_dtypes(include=['object']).columns.tolist(), '\n')

#Features with the MAX correlation
plt.figure(figsize=(12,10))        
corrMatrix = df.corr()
sn.heatmap(abs(corrMatrix), annot=True)  #displaying the absolute value of the correlations
plt.show()

###################### One-Hot Encoding ######################
OHE = pd.get_dummies(df['famhist'])
df = df.drop('famhist', axis=1)
df = df.join(OHE)
df.head()

#CorrMatrix after One-Hot Encoding
plt.figure(figsize=(12,10))        
corrMatrix = df.corr()
sn.heatmap(abs(corrMatrix), annot=True)  #displaying the absolute value of the correlations
plt.show()

###################### Standardization and Initialization ######################

df_x = df.iloc[:, np.r_[1:9, 10,11]].copy()
df_y = df.iloc[:,9].copy()

    
#Standardize Feature columns exced one-hot encoded ones
for col in df_x.columns[:-2]:
  df_x.loc[:,col] = Standardization (df_x.loc[:, col])
df_x.insert(0, 'x0', 1) #Add a column x0 to always be 1


#Random Initialization of Weights (btwn 0 and 1)
np.random.seed(10)
weights = np.random.rand(len(df_x.columns)) 

###################### Training ###################### 

#Case 1
a = 0.001
epoch = 500
size = len(df_x.index)

xx = [df_x.to_numpy()]
yy = [df_y.to_numpy()]

df_x = df_x.to_numpy()
df_y = df_y.to_numpy()
xx_len = len(xx)
err = []
iter_arr = []

for i in range(epoch):
  for j in range(xx_len):
    weights = Update (xx [j], yy[j], weights, a)
  err.append(CostFunc(weights, df_x, df_y))
  iter_arr.append(i)
        
plt.figure()
plt.scatter(iter_arr, err)
plt.xlabel ("Iteration Number")
plt.ylabel ("Error Rate")
plt.title ("Error Rate w/ Epoch = {}, Alpha = {}, Batch = {}".format(epoch, a, size))
plt.show()

###################### Accuracy Score ###################### 

probb = [] 

for i in range (size):
    for j in range (xx_len):
      probb.append(hypothesis (weights, xx[j]))

for i in range(len(probb)):
  if probb[i] >= 0.5:
    probb[i] = 1
  else:
    probb[i] = 0

probb = np.asarray(probb)
print ('\nAccuracy Score in (%): ', accuracy_score (df_y, probb)*100, '\n')

#Case 2

df_x = df.iloc[:, np.r_[1:9, 10,11]].copy()
df_y = df.iloc[:,9].copy()

    
#Standardize Feature columns exced one-hot encoded ones
for col in df_x.columns[:-2]:
  df_x.loc[:,col] = Standardization (df_x.loc[:, col])
df_x.insert(0, 'x0', 1) #Add a column x0 to always be 1


#Random Initialization of Weights (btwn 0 and 1)
np.random.seed(10)
weights = np.random.rand(len(df_x.columns)) 


a = 0.001
epoch = 1000
size = len(df_x.index)

xx = [df_x.to_numpy()]
yy = [df_y.to_numpy()]

df_x = df_x.to_numpy()
df_y = df_y.to_numpy()
xx_len = len(xx)
err = []
iter_arr = []

for i in range(epoch):
  for j in range(xx_len):
    weights = Update (xx [j], yy[j], weights, a)
  err.append(CostFunc(weights, df_x, df_y))
  iter_arr.append(i)
        
plt.figure()
plt.scatter(iter_arr, err)
plt.xlabel ("Iteration Number")
plt.ylabel ("Error Rate")
plt.title ("Error Rate w/ Epoch = {}, Alpha = {}, Batch = {}".format(epoch, a, size))
plt.show()

###################### Accuracy Score ###################### 

probb = [] 

for i in range (size):
    for j in range (xx_len):
      probb.append(hypothesis (weights, xx[j]))

for i in range(len(probb)):
  if probb[i] >= 0.5:
    probb[i] = 1
  else:
    probb[i] = 0

probb = np.asarray(probb)
print ('\nAccuracy Score in (%): ', accuracy_score (df_y, probb)*100, '\n')

#Case 3

df_x = df.iloc[:, np.r_[1:9, 10,11]].copy()
df_y = df.iloc[:,9].copy()

    
#Standardize Feature columns exced one-hot encoded ones
for col in df_x.columns[:-2]:
  df_x.loc[:,col] = Standardization (df_x.loc[:, col])
df_x.insert(0, 'x0', 1) #Add a column x0 to always be 1


#Random Initialization of Weights (btwn 0 and 1)
np.random.seed(10)
weights = np.random.rand(len(df_x.columns)) 


a = 0.001
epoch = 10000
size = len(df_x.index)

xx = [df_x.to_numpy()]
yy = [df_y.to_numpy()]

df_x = df_x.to_numpy()
df_y = df_y.to_numpy()
xx_len = len(xx)
err = []
iter_arr = []

for i in range(epoch):
  for j in range(xx_len):
    weights = Update (xx [j], yy[j], weights, a)
  err.append(CostFunc(weights, df_x, df_y))
  iter_arr.append(i)
        
plt.figure()
plt.scatter(iter_arr, err)
plt.xlabel ("Iteration Number")
plt.ylabel ("Error Rate")
plt.title ("Error Rate w/ Epoch = {}, Alpha = {}, Batch = {}".format(epoch, a, size))
plt.show()

###################### Accuracy Score ###################### 

probb = [] 

for i in range (size):
    for j in range (xx_len):
      probb.append(hypothesis (weights, xx[j]))

for i in range(len(probb)):
  if probb[i] >= 0.5:
    probb[i] = 1
  else:
    probb[i] = 0

probb = np.asarray(probb)
print ('\nAccuracy Score in (%): ', accuracy_score (df_y, probb)*100, '\n')

#Case 4

df_x = df.iloc[:, np.r_[1:9, 10,11]].copy()
df_y = df.iloc[:,9].copy()

    
#Standardize Feature columns exced one-hot encoded ones
for col in df_x.columns[:-2]:
  df_x.loc[:,col] = Standardization (df_x.loc[:, col])
df_x.insert(0, 'x0', 1) #Add a column x0 to always be 1


#Random Initialization of Weights (btwn 0 and 1)
np.random.seed(10)
weights = np.random.rand(len(df_x.columns)) 


a = 0.0001
epoch = 500
size = len(df_x.index)

xx = [df_x.to_numpy()]
yy = [df_y.to_numpy()]

df_x = df_x.to_numpy()
df_y = df_y.to_numpy()
xx_len = len(xx)
err = []
iter_arr = []

for i in range(epoch):
  for j in range(xx_len):
    weights = Update (xx [j], yy[j], weights, a)
  err.append(CostFunc(weights, df_x, df_y))
  iter_arr.append(i)
        
plt.figure()
plt.scatter(iter_arr, err)
plt.xlabel ("Iteration Number")
plt.ylabel ("Error Rate")
plt.title ("Error Rate w/ Epoch = {}, Alpha = {}, Batch = {}".format(epoch, a, size))
plt.show()

###################### Accuracy Score ###################### 

probb = [] 

for i in range (size):
    for j in range (xx_len):
      probb.append(hypothesis (weights, xx[j]))

for i in range(len(probb)):
  if probb[i] >= 0.5:
    probb[i] = 1
  else:
    probb[i] = 0

probb = np.asarray(probb)
print ('\nAccuracy Score in (%): ', accuracy_score (df_y, probb)*100, '\n')

#Case 5

#Split DataFrame into Feature and Target
df_x = df.iloc[:, np.r_[1:9, 10,11]].copy()
df_y = df.iloc[:,9].copy()

    
#Standardize Feature columns exced one-hot encoded ones
for col in df_x.columns[:-2]:
  df_x.loc[:,col] = Standardization (df_x.loc[:, col])
df_x.insert(0, 'x0', 1) #Add a column x0 to always be 1


#Random Initialization of Weights (btwn 0 and 1)
np.random.seed(10)
weights = np.random.rand(len(df_x.columns)) 


a = 0.0001
epoch = 1000
size = len(df_x.index)

xx = [df_x.to_numpy()]
yy = [df_y.to_numpy()]

df_x = df_x.to_numpy()
df_y = df_y.to_numpy()
xx_len = len(xx)
err = []
iter_arr = []

for i in range(epoch):
  for j in range(xx_len):
    weights = Update (xx [j], yy[j], weights, a)
  err.append(CostFunc(weights, df_x, df_y))
  iter_arr.append(i)
        
plt.figure()
plt.scatter(iter_arr, err)
plt.xlabel ("Iteration Number")
plt.ylabel ("Error Rate")
plt.title ("Error Rate w/ Epoch = {}, Alpha = {}, Batch = {}".format(epoch, a, size))
plt.show()

###################### Accuracy Score ###################### 

probb = [] 

for i in range (size):
    for j in range (xx_len):
      probb.append(hypothesis (weights, xx[j]))

for i in range(len(probb)):
  if probb[i] >= 0.5:
    probb[i] = 1
  else:
    probb[i] = 0

probb = np.asarray(probb)
print ('\nAccuracy Score in (%): ', accuracy_score (df_y, probb)*100, '\n')

#Case 6

df_x = df.iloc[:, np.r_[1:9, 10,11]].copy()
df_y = df.iloc[:,9].copy()

    
#Standardize Feature columns exced one-hot encoded ones
for col in df_x.columns[:-2]:
  df_x.loc[:,col] = Standardization (df_x.loc[:, col])
df_x.insert(0, 'x0', 1) #Add a column x0 to always be 1


#Random Initialization of Weights (btwn 0 and 1)
np.random.seed(10)
weights = np.random.rand(len(df_x.columns)) 


a = 0.0001
epoch = 10000
size = len(df_x.index)

xx = [df_x.to_numpy()]
yy = [df_y.to_numpy()]

df_x = df_x.to_numpy()
df_y = df_y.to_numpy()
xx_len = len(xx)
err = []
iter_arr = []

for i in range(epoch):
  for j in range(xx_len):
    weights = Update (xx [j], yy[j], weights, a)
  err.append(CostFunc(weights, df_x, df_y))
  iter_arr.append(i)
        
plt.figure()
plt.scatter(iter_arr, err)
plt.xlabel ("Iteration Number")
plt.ylabel ("Error Rate")
plt.title ("Error Rate w/ Epoch = {}, Alpha = {}, Batch = {}".format(epoch, a, size))
plt.show()

###################### Accuracy Score ###################### 

probb = [] 

for i in range (size):
    for j in range (xx_len):
      probb.append(hypothesis (weights, xx[j]))

for i in range(len(probb)):
  if probb[i] >= 0.5:
    probb[i] = 1
  else:
    probb[i] = 0

probb = np.asarray(probb)
print ('\nAccuracy Score in (%): ', accuracy_score (df_y, probb)*100, '\n')

#Case 7 - Mini-Batch 

#Split DataFrame into Feature and Target
df_x = df.iloc[:, np.r_[1:9, 10,11]].copy()
df_y = df.iloc[:,9].copy()

    
#Standardize Feature columns exced one-hot encoded ones
for col in df_x.columns[:-2]:
  df_x.loc[:,col] = Standardization (df_x.loc[:, col])
df_x.insert(0, 'x0', 1) #Add a column x0 to always be 1


#Random Initialization of Weights (btwn 0 and 1)
np.random.seed(10)
weights = np.random.rand(len(df_x.columns)) 


a = 0.001
epoch = 500
size = 50


xx = [df_x[i:i+size] for i in range(0, len(df_x),size)] #splits the data samples according to size indicated
xx = [i.to_numpy() for i in xx]
yy = [df_y[j:j+size] for j in range(0, len(df_y),size)]
yy = [j.to_numpy() for j in yy]

df_x = df_x.to_numpy()
df_y = df_y.to_numpy()
xx_len = len(xx)
err = []
iter_arr = []

for i in range(epoch):
  for j in range(xx_len):
    weights = Update (xx [j], yy[j], weights, a)
  err.append(CostFunc(weights, df_x, df_y))
  iter_arr.append(i)
        
plt.figure()
plt.scatter(iter_arr, err)
plt.xlabel ("Iteration Number")
plt.ylabel ("Error Rate")
plt.title ("Error Rate (Epoch = {}, Alpha = {}, Batch = {})".format(epoch, a, size))


###################### Accuracy Score ###################### 

probb = [] 

for i in range (462):
  probb.append(hypothesis (weights, xx[j]))

for i in range(len(probb)):
  if probb[i] >= 0.5:
    probb[i] = 1
  else:
    probb[i] = 0

probb = np.asarray(probb)
print ('\nAccuracy Score in (%): ', accuracy_score (df_y, probb)*100, '\n')

#Case 8 - Mini-Batch 

df_x = df.iloc[:, np.r_[1:9, 10,11]].copy()
df_y = df.iloc[:,9].copy()

    
#Standardize Feature columns exced one-hot encoded ones
for col in df_x.columns[:-2]:
  df_x.loc[:,col] = Standardization (df_x.loc[:, col])
df_x.insert(0, 'x0', 1) #Add a column x0 to always be 1


#Random Initialization of Weights (btwn 0 and 1)
np.random.seed(10)
weights = np.random.rand(len(df_x.columns)) 


a = 0.001
epoch = 1000
size = 50

xx = [df_x[i:i+size] for i in range(0, len(df_x),size)] #splits the data samples according to size indicated
xx = [i.to_numpy() for i in xx]
yy = [df_y[j:j+size] for j in range(0, len(df_y),size)]
yy = [j.to_numpy() for j in yy]

df_x = df_x.to_numpy()
df_y = df_y.to_numpy()
xx_len = len(xx)
err = []
iter_arr = []

for i in range(epoch):
  for j in range(xx_len):
    weights = Update (xx [j], yy[j], weights, a)
  err.append(CostFunc(weights, df_x, df_y))
  iter_arr.append(i)
        
plt.figure()
plt.scatter(iter_arr, err)
plt.xlabel ("Iteration Number")
plt.ylabel ("Error Rate")
plt.title ("Error Rate (Epoch = {}, Alpha = {}, Batch = {})".format(epoch, a, size))

###################### Accuracy Score ###################### 

probb = [] 

for i in range (462):
  probb.append(hypothesis (weights, xx[j]))

for i in range(len(probb)):
  if probb[i] >= 0.5:
    probb[i] = 1
  else:
    probb[i] = 0

probb = np.asarray(probb)
print ('\nAccuracy Score in (%): ', accuracy_score (df_y, probb)*100, '\n')

#Case 9 - Mini-Batch 

#Split DataFrame into Feature and Target
df_x = df.iloc[:, np.r_[1:9, 10,11]].copy()
df_y = df.iloc[:,9].copy()

    
#Standardize Feature columns exced one-hot encoded ones
for col in df_x.columns[:-2]:
  df_x.loc[:,col] = Standardization (df_x.loc[:, col])
df_x.insert(0, 'x0', 1) #Add a column x0 to always be 1


#Random Initialization of Weights (btwn 0 and 1)
np.random.seed(10)
weights = np.random.rand(len(df_x.columns)) 


a = 0.001
epoch = 10000
size = 50

xx = [df_x[i:i+size] for i in range(0, len(df_x),size)] #splits the data samples according to size indicated
xx = [i.to_numpy() for i in xx]
yy = [df_y[j:j+size] for j in range(0, len(df_y),size)]
yy = [j.to_numpy() for j in yy]

df_x = df_x.to_numpy()
df_y = df_y.to_numpy()
xx_len = len(xx)
err = []
iter_arr = []

for i in range(epoch):
  for j in range(xx_len):
    weights = Update (xx [j], yy[j], weights, a)
  err.append(CostFunc(weights, df_x, df_y))
  iter_arr.append(i)
        
plt.figure()
plt.scatter(iter_arr, err)
plt.xlabel ("Iteration Number")
plt.ylabel ("Error Rate")
plt.title ("Error Rate (Epoch = {}, Alpha = {}, Batch = {})".format(epoch, a, size))

###################### Accuracy Score ###################### 

probb = [] 

for i in range (462):
  probb.append(hypothesis (weights, xx[j]))

for i in range(len(probb)):
  if probb[i] >= 0.5:
    probb[i] = 1
  else:
    probb[i] = 0

probb = np.asarray(probb)
print ('\nAccuracy Score in (%): ', accuracy_score (df_y, probb)*100, '\n')

#Case 10 - Mini-Batch 

#Split DataFrame into Feature and Target
df_x = df.iloc[:, np.r_[1:9, 10,11]].copy()
df_y = df.iloc[:,9].copy()

    
#Standardize Feature columns exced one-hot encoded ones
for col in df_x.columns[:-2]:
  df_x.loc[:,col] = Standardization (df_x.loc[:, col])
df_x.insert(0, 'x0', 1) #Add a column x0 to always be 1


#Random Initialization of Weights (btwn 0 and 1)
np.random.seed(10)
weights = np.random.rand(len(df_x.columns)) 


a = 0.0001
epoch = 500
size = 50

xx = [df_x[i:i+size] for i in range(0, len(df_x),size)] #splits the data samples according to size indicated
xx = [i.to_numpy() for i in xx]
yy = [df_y[j:j+size] for j in range(0, len(df_y),size)]
yy = [j.to_numpy() for j in yy]

df_x = df_x.to_numpy()
df_y = df_y.to_numpy()
xx_len = len(xx)
err = []
iter_arr = []

for i in range(epoch):
  for j in range(xx_len):
    weights = Update (xx [j], yy[j], weights, a)
  err.append(CostFunc(weights, df_x, df_y))
  iter_arr.append(i)
        
plt.figure()
plt.scatter(iter_arr, err)
plt.xlabel ("Iteration Number")
plt.ylabel ("Error Rate")
plt.title ("Error Rate (Epoch = {}, Alpha = {}, Batch = {})".format(epoch, a, size))


###################### Accuracy Score ###################### 

probb = [] 

for i in range (462):
  probb.append(hypothesis (weights, xx[j]))

for i in range(len(probb)):
  if probb[i] >= 0.5:
    probb[i] = 1
  else:
    probb[i] = 0

probb = np.asarray(probb)
print ('\nAccuracy Score in (%): ', accuracy_score (df_y, probb)*100, '\n')

#Case 11 - Mini-Batch 

#Split DataFrame into Feature and Target
df_x = df.iloc[:, np.r_[1:9, 10,11]].copy()
df_y = df.iloc[:,9].copy()

    
#Standardize Feature columns exced one-hot encoded ones
for col in df_x.columns[:-2]:
  df_x.loc[:,col] = Standardization (df_x.loc[:, col])
df_x.insert(0, 'x0', 1) #Add a column x0 to always be 1


#Random Initialization of Weights (btwn 0 and 1)
np.random.seed(10)
weights = np.random.rand(len(df_x.columns)) 

a = 0.0001
epoch = 1000
size = 50

xx = [df_x[i:i+size] for i in range(0, len(df_x),size)] #splits the data samples according to size indicated
xx = [i.to_numpy() for i in xx]
yy = [df_y[j:j+size] for j in range(0, len(df_y),size)]
yy = [j.to_numpy() for j in yy]

df_x = df_x.to_numpy()
df_y = df_y.to_numpy()
xx_len = len(xx)
err = []
iter_arr = []

for i in range(epoch):
  for j in range(xx_len):
    weights = Update (xx [j], yy[j], weights, a)
  err.append(CostFunc(weights, df_x, df_y))
  iter_arr.append(i)
        
plt.figure()
plt.scatter(iter_arr, err)
plt.xlabel ("Iteration Number")
plt.ylabel ("Error Rate")
plt.title ("Error Rate (Epoch = {}, Alpha = {}, Batch = {})".format(epoch, a, size))

###################### Accuracy Score ###################### 

probb = [] 

for i in range (462):
  probb.append(hypothesis (weights, xx[j]))

for i in range(len(probb)):
  if probb[i] >= 0.5:
    probb[i] = 1
  else:
    probb[i] = 0

probb = np.asarray(probb)
print ('\nAccuracy Score in (%): ', accuracy_score (df_y, probb)*100, '\n')

#Case 12 - Mini-Batch 

#Split DataFrame into Feature and Target
df_x = df.iloc[:, np.r_[1:9, 10,11]].copy()
df_y = df.iloc[:,9].copy()

    
#Standardize Feature columns exced one-hot encoded ones
for col in df_x.columns[:-2]:
  df_x.loc[:,col] = Standardization (df_x.loc[:, col])
df_x.insert(0, 'x0', 1) #Add a column x0 to always be 1


#Random Initialization of Weights (btwn 0 and 1)
np.random.seed(10)
weights = np.random.rand(len(df_x.columns)) 


a = 0.0001
epoch = 10000
size = 50

xx = [df_x[i:i+size] for i in range(0, len(df_x),size)] #splits the data samples according to size indicated
xx = [i.to_numpy() for i in xx]
yy = [df_y[j:j+size] for j in range(0, len(df_y),size)]
yy = [j.to_numpy() for j in yy]

df_x = df_x.to_numpy()
df_y = df_y.to_numpy()
xx_len = len(xx)
err = []
iter_arr = []

for i in range(epoch):
  for j in range(xx_len):
    weights = Update (xx [j], yy[j], weights, a)
  err.append(CostFunc(weights, df_x, df_y))
  iter_arr.append(i)
        
plt.figure()
plt.scatter(iter_arr, err)
plt.xlabel ("Iteration Number")
plt.ylabel ("Error Rate")
plt.title ("Error Rate (Epoch = {}, Alpha = {}, Batch = {})".format(epoch, a, size))


###################### Accuracy Score ###################### 

probb = [] 

for i in range (462):
  probb.append(hypothesis (weights, xx[j]))

for i in range(len(probb)):
  if probb[i] >= 0.5:
    probb[i] = 1
  else:
    probb[i] = 0

probb = np.asarray(probb)
print ('\nAccuracy Score in (%): ', accuracy_score (df_y, probb)*100, '\n')

# Logistic Regression implementation with Sklearn Library

import pandas as pd
import numpy as np
import io
from google.colab import files
from sklearn.linear_model import LogisticRegression
from sklearn import metrics

uploaded = files.upload()
df = pd.read_csv(io.BytesIO(uploaded['heart.csv']))


def Standardization (df):  
  mean = np.mean(df)
  std = np.std(df)
  return [(i-mean)/std for i in df]

###################### One-Hot Encoding ######################
OHE = pd.get_dummies(df['famhist'])
df = df.drop('famhist', axis=1)
df = df.join(OHE)

#Split DataFrame into Feature and Target
df_x = df.iloc[:, np.r_[1:9, 10,11]].copy()
df_y = df.iloc[:,9].copy()
    
#Standardize Feature columns exced one-hot encoded ones
for col in df_x.columns[:-2]:
  df_x.loc[:,col] = Standardization (df_x.loc[:, col])
df_x.insert(0, 'x0', 1) #Add a column x0 to always be 1


regressor = LogisticRegression() 
regressor.fit(df_x, df_y) 
y_pred = regressor.predict (df_x)
coeffs = np.hstack((model.coef_, model.intercept_[:, None]))
print ('\nThe latest coefficients are: \n', coeffs)

accuracy = metrics.accuracy_score (df_y, y_pred)
print ('Accuracy in (%): ', 100*accuracy)